const filter = (array, f) => {
  let filteredArray = [];
  for (let i = 0; i < array.length; i++) {
    if (f(array[i])) {
      filteredArray.push(array[i]);
    }
  }
  return filteredArray;
}

exports.filter = filter;