exports.getProfit_n2 = function getProfit_n2(prices) {
  let maxProfit = Number.MIN_SAFE_INTEGER;
  for (let i = 0; i < prices.length - 1; i++) {
    for (let j = i + 1; j < prices.length; j++) {
      const buy = prices[i];
      const sell = prices[j];
      const profit = sell - buy;

      if (profit > maxProfit) {
        maxProfit = profit;
      }
    }
  }
  return maxProfit;
};

exports.getProfit_n = function getProfit_n(prices) {
  if (prices.length < 2) return null;
  if (prices.length === 2) return prices[1] - prices[0];

  let buyTime = 0;
  let sellTime = 1;
  let min = buyTime;

  for (let i = 2; i < prices.length; i++) {
    if (prices[i - 1] < prices[min]) min = i - 1;
    if (prices[i] - prices[min] > prices[sellTime] - prices[buyTime]) {
      sellTime = i;
      buyTime = min;
    }
  }

  return prices[sellTime] - prices[buyTime];
}