const map = (array, f) => {
  let result = [];
  for (let i = 0; i < array.length; i++) {
    result.push(f(array[i]));
  }
  return result;
}

exports.map = map;