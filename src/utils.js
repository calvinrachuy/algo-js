exports.sortNumeric = function sortNumeric(list) {
  return list.sort((a, b) => a - b);
}
