/*
Two Sum, but the supplied list of numbers is sorted.

** CATCH: return results 1-indexed **
Note: results must be in correct order
*/

// binary search on remaining list adds time and logical complexity

// Turns out it's a ... two pointer solution

const { binarySearch } = require('./binarySearch.js');

const twoSumSorted = (nums, target) => {
  let i = 0;
  let j = nums.length - 1;

  while (i < j) {
    let sum = nums[i] + nums[j];

    if (sum === target) {
      break;
    }

    if (sum < target) {
      i++;
    }
    if (sum > target) {
      j--;
    }
  }

  return [i + 1, j + 1];
}

exports.twoSumSorted = twoSumSorted;