const filterUnique = list => {
  let map = {};
  let unique = [];
  list.forEach(v => {
    if (!map.hasOwnProperty(v)) {
      map[v] = true;
      unique.push(v);
    }
  })
  return unique;
}

exports.filterUnique = filterUnique;