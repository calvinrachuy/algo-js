/*
Runtime: 166 ms, faster than 5.82% of JavaScript online submissions for Intersection of Two Arrays.
Memory Usage: 43.9 MB, less than 5.38% of JavaScript online submissions for Intersection of Two Arrays.
*/
// big and slow
const intersectionUsingSets = (a, b) => [... new Set(b.filter(n => new Set(a).has(n)))];

/*
Runtime: 81 ms, faster than 39.27% of JavaScript online submissions for Intersection of Two Arrays.
Memory Usage: 40.2 MB, less than 60.60% of JavaScript online submissions for Intersection of Two Arrays.
*/
// less space, interestingly
const intersection = (a, b) => {
  let aMap = {};
  let bMap = {};
  a.forEach(n => aMap[n] = true);
  b.forEach(n => bMap[n] = true);
  return Object.keys(bMap).map(n => parseInt(n)).filter(n => aMap[n]);
}

/*
Runtime: 85 ms, faster than 28.52% of JavaScript online submissions for Intersection of Two Arrays.
Memory Usage: 40 MB, less than 73.61% of JavaScript online submissions for Intersection of Two Arrays.
*/
const intersectionUsingOneSet = (a, b) => {
  let result = [];
  let A = new Set(a);
  let B = new Set(b);
  B.forEach(n => {
    if (A.has(n)) {
      result.push(n);
    }
  })
  return result;
}

exports.intersection = intersection;