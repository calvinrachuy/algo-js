/*
Runtime: 68 ms, faster than 98.19% of JavaScript online submissions for Binary Search.
Memory Usage: 42.7 MB, less than 11.20% of JavaScript online submissions for Binary Search.
*/

const binarySearch = (list, target) => {
  let left = 0;
  let right = list.length - 1;

  while (left <= right) {
    let i = Math.floor((left + right) / 2);

    if (list[i] === target) return i;

    if (list[i] < target) {
      left = i + 1;
    }
    else {
      right = i - 1;
    }
  }

  return -1;
}

exports.binarySearch = binarySearch;