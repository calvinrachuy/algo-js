/*
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Output: Because nums[0] + nums[1] == 9, we return [0, 1].
*/

// O(n^2)
// nested loop
// Try every pair

// O(2n) => O(n)
// loop once to build map
// loop again to compare

// O(n)
// loop once
// build map and compare
const twoSum = (nums, target) => {
  let map = {};

  for (let i = 0; i < nums.length; i++) {
    const a = nums[i]
    const b = target - a;

    if (map.hasOwnProperty(b)) {
      return [i, map[b]];
    }

    map[a] = i;
  }
}

exports.twoSum = twoSum;