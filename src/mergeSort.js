const mergeSort = list => {
  if (list.length < 2) return list;

  const i = Math.floor(list.length / 2);
  let left = mergeSort(list.slice(0, i));
  let right = mergeSort(list.slice(i, list.length));

  let l = 0;
  let r = 0;
  let sorted = [];
  while (l < left.length && r < right.length) {
    if (left[l] <= right[r]) {
      sorted.push(left[l]);
      l++;
    }
    if (right[r] < left[l]) {
      sorted.push(right[r]);
      r++;
    }
  }
  return sorted.concat(left.slice(l, left.length)).concat(right.slice(r, right.length));
}

exports.mergeSort = mergeSort;