// https://leetcode.com/problems/find-all-duplicates-in-an-array/

// works if each value only present a maximum of twice
// const findDuplicates = nums => {
//   let found = {};
//   let duplicates = [];
//   nums.forEach(n => {
//     if (found[n]) {
//       duplicates.push(n);
//     }
//     else {
//       found[n] = true;
//     }
//   })
//   return duplicates;
// }

const findDuplicates = nums => {
  let counts = {};

  nums.forEach(n => {
    if (counts[n]) {
      counts[n]++;
    }
    else {
      counts[n] = 1;
    }
  })

  return Object.keys(counts).map(k => parseInt(k)).filter(k => counts[k] > 1);
}

exports.findDuplicates = findDuplicates;