const { sortNumeric } = require('./utils.js');

const cafeteria = (N, K, M, S) => {
  function sortNumeric(list) {
    return list.sort((a, b) => a - b);
  }

  let count = 0;
  let seats = sortNumeric(S);

  count += Math.floor((seats[0] - 1) / (K + 1));
  for (let i = 1; i < seats.length; i++) {
    count += Math.floor((seats[i] - seats[i - 1] - 1 - K) / (K + 1));
  }
  count += Math.floor((N - seats[seats.length - 1]) / (K + 1));

  return count;
}

const cafeteriaDataStructure = (N, K, M, S) => {
  // Represent the table
  let seats = new Array(N).fill(null);
  let count = 0;

  const fillRight = (seats, s, K) => {
    // Reserve space K to the right
    let b = s + 1;
    while (b < seats.length && b <= s + K) {
      if (seats[b] !== null) break;
      seats[b] = 0;
      b++;
    }
  }

  // Fill known seats
  // This function would be exported and tested
  S.forEach(s => {
    // The designations start at 1
    // Start at zero
    s = s - 1;
    seats[s] = 1;

    // Reserve space K to the left
    let a = s - 1;
    while (a >= 0 && a >= s - K) {
      if (seats[a] !== null) break;
      seats[a] = 0;
      a--;
    }

    fillRight(seats, s, K);
  })

  for (let i = 0; i < seats.length; i++) {
    if (seats[i] === null) {
      count++
      seats[i] = 1;
      fillRight(seats, i, K);
    }
  }

  return count;
}

exports.cafeteria = cafeteria;
exports.cafeteriaDataStructure = cafeteriaDataStructure;
