const assert = require('chai').assert;
const { sortNumeric } = require('../src/utils.js');
const { mergeSort } = require('../src/mergeSort.js');

describe('mergeSort', function () {
  it('sorts', function () {
    const input = [3, 5, 1, -1, 0, 5, 11];
    const expected = sortNumeric(Array.from(input));

    const actual = mergeSort(input);

    assert.sameOrderedMembers(actual, expected);
  })
})