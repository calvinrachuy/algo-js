const assert = require('chai').assert;

const { cafeteria, cafeteriaDataStructure } = require('../src/cafeteria');

describe('cafeteria', function () {
  it('case 1', function () {
    const N = 10
    const K = 1;
    const M = 2;
    const S = [2, 6];
    const expected = 3;

    const actual = cafeteria(N, K, M, S);

    assert.strictEqual(actual, expected);
  })

  it('case 2', function () {
    const N = 15
    const K = 2
    const M = 3
    const S = [11, 6, 14]
    const expected = 1

    const actual = cafeteria(N, K, M, S);

    assert.strictEqual(actual, expected);
  })
})

describe('cafeteriaDataStructure', function () {
  it('case 1', function () {
    const N = 10
    const K = 1;
    const M = 2;
    const S = [2, 6];
    const expected = 3;

    const actual = cafeteriaDataStructure(N, K, M, S);

    assert.strictEqual(actual, expected);
  })

  it('case 2', function () {
    const N = 15
    const K = 2
    const M = 3
    const S = [11, 6, 14]
    const expected = 1

    const actual = cafeteriaDataStructure(N, K, M, S);

    assert.strictEqual(actual, expected);
  })
})