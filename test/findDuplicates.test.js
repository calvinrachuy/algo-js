// https://leetcode.com/problems/find-all-duplicates-in-an-array/

const assert = require('chai').assert;
const { findDuplicates } = require('../src/findDuplicates');

describe('findDuplicates', function () {
  it('LeetCode case 1', function () {
    const nums = [4, 3, 2, 7, 8, 2, 3, 1];
    const expected = [2, 3];

    const actual = findDuplicates(nums);

    assert.sameMembers(actual, expected);
  })

  it('LeetCode case 2', function () {
    const nums = [1, 1, 2];
    const expected = [1];

    const actual = findDuplicates(nums);

    assert.sameMembers(actual, expected);
  })

  it('LeetCode case 3', function () {
    const nums = [1];
    const expected = [];

    const actual = findDuplicates(nums);

    assert.sameMembers(actual, expected);
  })

  it('occurences over 2', function () {
    const nums = [4, 3, 2, 7, 8, 2, 3, 2, 3, 3, 1];
    const expected = [2, 3];

    const actual = findDuplicates(nums);

    assert.sameMembers(actual, expected);
  })
})