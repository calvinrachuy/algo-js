const assert = require('chai').assert;
const { twoSum } = require('../src/twoSum.js');

describe('two sum', function () {

  it('function is defined', function () {
    assert.isFunction(twoSum);
  })

  it('returns an array of length two', function () {
    const result = twoSum([0, 1, 2, 3], 3);

    assert.strictEqual(result.length, 2);
  })

  it('case 1', function () {
    const nums = [2, 7, 11, 15];
    const target = 9;
    const expected = [0, 1];

    const actual = twoSum(nums, target);

    assert.sameMembers(actual, expected);
  })

  it('case 2', function () {
    const nums = [3, 2, 4];
    const target = 6;
    const expected = [1, 2];

    const actual = twoSum(nums, target);

    assert.sameMembers(actual, expected);
  })
})