const assert = require('chai').assert;
const { intersection } = require('../src/intersectionOfArrays.js');

describe('intersectionOfArrays', function () {
  it('LeetCode 1', function () {
    const nums1 = [1, 2, 2, 1];
    const nums2 = [2, 2];
    const expected = [2];

    const actual = intersection(nums1, nums2);

    assert.sameMembers(actual, expected);
  })

  it('LeetCode 1', function () {
    const nums1 = [4, 9, 5];
    const nums2 = [9, 4, 9, 8, 4];
    const expected = [4, 9];

    const actual = intersection(nums1, nums2);

    assert.sameMembers(actual, expected);
  })
})