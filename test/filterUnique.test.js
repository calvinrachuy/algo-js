const assert = require('chai').assert;
const { filterUnique } = require('../src/filterUnique.js');

describe('filterUnique', function () {
  it('returns a new array without duplicates', function () {
    const list = [1, 4, 1, 3, 4, 2, 6, 2, 1];
    const expected = [1, 4, 3, 2, 6];

    const actual = filterUnique(list);

    assert.sameOrderedMembers(actual, expected);
    assert.sameOrderedMembers([1, 4, 1, 3, 4, 2, 6, 2, 1], list);
  })
})