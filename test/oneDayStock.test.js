const assert = require('chai').assert;

const solutions = require('../src/oneDayStock.js');

describe('oneDayStock', function () {
  [
    solutions.getProfit_n2,
    solutions.getProfit_n,
  ].forEach(function (solution) {
    describe(solution.name, function () {
      it('naive case', function () {
        const prices = [1, 5];
        const result = solution(prices);

        assert.strictEqual(result, 4);
      })

      it('given case', function () {
        const prices = [10, 7, 5, 8, 11, 9];

        assert.strictEqual(solution(prices), 6);
      })
    })
  })
});