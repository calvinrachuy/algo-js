const assert = require('chai').assert;
const { sortNumeric } = require('../src/utils.js');

describe('sortNumeric', function () {
  it('sorts', function () {
    assert.deepEqual(sortNumeric([9, 1, -2, 0, 1, 10]), [-2, 0, 1, 1, 9, 10])
  })
})