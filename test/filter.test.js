const assert = require('chai').assert;

const { filter } = require('../src/filter.js');

describe('filter', function () {
  it('returns a new array filtered by the supplied function', function () {
    const list = [1, 2, 3, 4, 5];
    const onlyEvens = n => n % 2 === 0;
    const expected = [2, 4];

    const actual = filter(list, onlyEvens);

    assert.sameOrderedMembers(actual, expected);
  })
})