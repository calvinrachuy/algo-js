const assert = require('chai').assert;

const { binarySearch } = require('../src/binarySearch.js');

describe('binarySearch', function () {
  it('returns -1 if not found', function () {
    const nums = [1, 3, 5, 10];
    const target = 11;
    const expected = -1;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })

  it('single element', function () {
    const nums = [3];
    const target = 3;
    const expected = 0;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })

  it('short case', function () {
    const nums = [1, 3, 5];
    const target = 3;
    const expected = 1;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })


  it('short case 2', function () {
    const nums = [1, 3, 5, 10];
    const target = 5;
    const expected = 2;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })

  it('handles low', function () {
    const nums = [1, 3, 5, 6, 10, 20, 22];
    const target = 3;
    const expected = 1;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })

  it('handles first', function () {
    const nums = [1, 3, 5, 6, 10, 20, 22];
    const target = 1;
    const expected = 0;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })

  it('handles high', function () {
    const nums = [1, 3, 5, 6, 10, 20, 22];
    const target = 22;
    const expected = 6;

    const actual = binarySearch(nums, target);

    assert.strictEqual(actual, expected);
  })
})