const assert = require('chai').assert;

const { forEach } = require('../src/forEach.js');

describe('forEach', function () {
  it('calls the passed function on every element', function () {
    let voteCount = { red: 0, blue: 0 }
    const voteList = ['red', 'red', 'blue', 'red', 'blue', 'red']

    forEach(voteList, v => voteCount[v]++);

    assert.deepEqual(voteCount, { red: 4, blue: 2 });
  })
})