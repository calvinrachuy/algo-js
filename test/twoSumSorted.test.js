const assert = require('chai').assert;

const { twoSumSorted } = require('../src/twoSumSorted.js');

describe('twoSumSorted', function () {
  it('is a function', function () {
    assert.isFunction(twoSumSorted);
  })

  it('returns array length 2', function () {
    const actual = twoSumSorted([9, 9, 9], 99);

    assert.isArray(actual);
    assert.strictEqual(actual.length, 2);
  })

  it('case 1', function () {
    const nums = [2, 7, 11, 15];
    const target = 9;
    const expected = [1, 2];

    const actual = twoSumSorted(nums, target);

    assert.sameMembers(actual, expected);
  })

  it('case 2', function () {
    const nums = [2, 3, 4];
    const target = 6;
    const expected = [1, 3];

    const actual = twoSumSorted(nums, target);

    assert.sameMembers(actual, expected);
  })
})