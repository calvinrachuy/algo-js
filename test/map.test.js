const assert = require('chai').assert;

const { map } = require('../src/map.js');

describe('map', function () {
  it('calls the function on every element and returns a new array', function () {
    const list = [1, 2, 3, 4];
    const square = n => n ** 2;
    const expected = [1, 4, 9, 16];

    const actual = map(list, square);

    assert.sameOrderedMembers(actual, expected);
    assert.notEqual(actual, expected);
    assert.sameOrderedMembers(list, [1, 2, 3, 4])
  })
})