const assert = require('chai').assert;

describe('chai check', function () {
  it('does some basic checks', function () {
    assert.strictEqual(1, 1);
    assert.notEqual(1, 2);
    assert.isTrue(true);
    assert.isNotTrue(false);
    assert.isFalse(false);
    assert.isNull(null);
    assert.isUndefined(undefined);
    assert.isFalse(!true);
    assert.isTrue(!false);
    assert.isNumber(2);
    assert.isNotNumber('2');
    assert.isFunction(function () { });
    assert.isNotFunction({});
    assert.isObject({});
  })

  it('equal', function () {
    assert.equal(1, '1');
    assert.notEqual(1, '2');
    assert.notEqual(1, 2);
  })

  it('objects are not simple equal by shape', function () {
    assert.notEqual({ milk: 'cookies' }, { milk: 'cookies' });
  })

  it('array is simple equal by reference', function () {
    const array = [1, 2, 3]
    assert.equal(array, array);
  })

  it('array is not simple equal by values', function () {
    const array = [1, 2, 3]
    assert.notEqual(array, [1, 2, 3]);
  })

  it('strictEqual', function () {
    assert.strictEqual(1, 1);
    assert.notStrictEqual(1, '1');
    assert.notStrictEqual(1, 2);
    assert.notStrictEqual('false', false);
  })

  it('deepEqual', function () {
    assert.deepEqual({ milk: 'cookies' }, { milk: 'cookies' });

    const obj = { milk: 'cookies' };
    const obj2 = Object.assign({}, obj);
    assert.deepEqual(obj, obj2);
  })

  it('deepEqual not ordered', function () {
    const bob = { id: 10, name: { first: 'bob', last: 'jones' }, phoneNumber: '555-123-4567' };
    let jones = { name: { last: 'jones' }, id: 10, phoneNumber: '555-123-4567' };
    jones.name.first = 'bob';

    assert.notEqual(JSON.stringify(bob), JSON.stringify(jones));
    assert.deepEqual(bob, jones);
  })

  it('sameMembers', function () {
    assert.sameMembers([1, 2, 3], [3, 1, 2]);
    assert.sameMembers([1, 2, 3, 'foo'], [3, 1, 2, 'foo']);

    assert.notSameMembers([1, 3], [3, 1, 2]);
    assert.notSameMembers([1, 2, 3], [1, 2]);
    assert.notSameMembers([1, 2, '3'], [3, 1, 2]);
  })

  it('sameOrderedMembers', function () {
    assert.sameOrderedMembers([1, 2, 3], [1, 2, 3]);
    assert.sameOrderedMembers([1, 2, 3, 'foo'], [1, 2, 3, 'foo']);

    assert.notSameOrderedMembers([1, 2, 3], [3, 1, 2]);
    assert.notSameOrderedMembers([1, 2, 3], [1, 2]);
    assert.notSameOrderedMembers([1, 2, '3'], [1, 2, 3]);
  })
})