const assert = require('chai').assert;

const { maxProfit } = require('../src/multipleDayStock.js');

xdescribe('multipleDayStock', function () {
  it('case 1', function () {
    const input = [7, 1, 5, 3, 6, 4];
    const expected = 7;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('case 2', function () {
    const input = [1, 2, 3, 4, 5];
    const expected = 4;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('empty', function () {
    const input = [];
    const expected = 0;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('single', function () {
    const input = [1];
    const expected = 0;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('no gains', function () {
    const input = [10, 1];
    const expected = 0;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('short gain', function () {
    const input = [1, 6];
    const expected = 5;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })

  it('simple gain', function () {
    const input = [4, 5, 9];
    const expected = 5;

    const actual = maxProfit(input);

    assert.strictEqual(actual, expected);
  })
})