const assert = require('chai').assert;

const { removeDuplicatesSorted } = require('../src/removeDuplicatesSorted.js');
const f = removeDuplicatesSorted;

describe('removeDuplicatesSorted', function () {
  it('handles empty', function () {
    const input = [];
    const expected = 0;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })

  it('handles single', function () {
    const input = [];
    const expected = 0;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })

  it('handles short unique', function () {
    const input = [0];
    const expected = 1;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })

  it('handles short all duplicate', function () {
    const input = [1, 1, 1];
    const expected = 1;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })

  it('handles short typical', function () {
    const input = [1, 1, 2];
    const expected = 2;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })

  it('handles typical', function () {
    const input = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4];
    const expected = 5;

    const actual = f(input);

    assert.strictEqual(actual, expected);
  })
})